"use strict";

require("core-js/modules/es.weak-map.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Main = exports.LiRow = exports.Button = exports.BottomDiv = void 0;
exports.Pagination = Pagination;
exports.UlRow = exports.ThRow = exports.TdRow = exports.Table = exports.THead = exports.Select = void 0;
exports.default = ReactDataTable;
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.array.sort.js");
var _react = _interopRequireWildcard(require("react"));
var _styledComponents = _interopRequireDefault(require("styled-components"));
var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11, _templateObject12, _templateObject13, _templateObject14, _templateObject15, _templateObject16, _templateObject17;
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }
/* A function that is exported. */
function ReactDataTable(payload) {
  const [currentPage, setCurrentPage] = (0, _react.useState)(0);
  const [recordsPerPage, setRecordsPerPage] = (0, _react.useState)(10);
  const [active, setActive] = (0, _react.useState)(0);
  const [data, setData] = (0, _react.useState)(payload.data);
  const [headers, setHeaders] = (0, _react.useState)(payload.headers);
  let isError = false;
  if (payload.hasOwnProperty('headers') && payload.hasOwnProperty('data')) {
    if (payload.data.length > 0 && payload.headers.length !== payload.data[0].length) {
      isError = true;
      console.log('Données non conformes');
    }
  }
  if (payload.debug) console.log('headers :', headers, 'data : ', data);
  function searchBy(searchTerm) {
    if (searchTerm !== '') {
      const filteredData = data.filter(record => record.some(value => value.toString().includes(searchTerm)));
      setData(filteredData);
    } else {
      setData(payload.data);
    }
  }
  function sortBy(element) {
    if (payload.debug) console.log(element, headers);
    headers.forEach(h => {
      if (h.name === element.name) {
        h.isActive = true;
      } else {
        h.isActive = false;
      }
    });
    const indexToSort = headers.indexOf(element);
    element.sens *= -1;
    const sens = element.sens;
    const type = element.type;
    if (type === 'string') {
      data.sort((a, b) => sens * a[indexToSort].localeCompare(b[indexToSort]));
    } else if (type === 'number') {
      data.sort((a, b) => sens * (a[indexToSort] - b[indexToSort]));
    } else if (type === 'boolean') {
      data.sort((a, b) => sens * a[indexToSort] - b[indexToSort]);
      if (payload.debug) console.log('triage');
    }
    setData(data);
    setHeaders(headers);
    setActive(active + 1);
  }
  return /*#__PURE__*/_react.default.createElement(Main, null, /*#__PURE__*/_react.default.createElement(ContainerInput, null, /*#__PURE__*/_react.default.createElement(InputSearch, {
    onChange: e => searchBy(e.target.value)
  })), data && data.length > 0 && !isError ? /*#__PURE__*/_react.default.createElement(Table, null, /*#__PURE__*/_react.default.createElement(THead, null, /*#__PURE__*/_react.default.createElement("tr", null, headers === null || headers === void 0 ? void 0 : headers.map((element, index) => {
    return /*#__PURE__*/_react.default.createElement(ThRow, {
      key: index,
      "data-testid": "h".concat(index),
      style: element.isActive ? {
        backgroundColor: '#1F3140',
        color: 'white',
        borderBottom: '5px solid #09A',
        textUnderlineOffset: '8px',
        textDecorationThickness: '3px',
        boxSizing: 'border-box'
      } : {
        backgroundColor: '#1F3140',
        borderBottom: '5px solid transparent'
      }
    }, element.name, ' ', /*#__PURE__*/_react.default.createElement(Button, {
      "data-testid": "but".concat(index),
      onClick: () => sortBy(element)
    }, element.sens === 1 ? /*#__PURE__*/_react.default.createElement("span", {
      style: {
        color: 'white'
      }
    }, "\u25B2") : /*#__PURE__*/_react.default.createElement("span", {
      style: {
        color: 'white'
      }
    }, "\u25BC")), ' ');
  }))), /*#__PURE__*/_react.default.createElement("tbody", null, data.slice(currentPage * recordsPerPage, Math.min(data.length, (currentPage + 1) * recordsPerPage)).map((e, indx) => {
    return /*#__PURE__*/_react.default.createElement("tr", {
      key: indx,
      style: indx % 2 === 0 ? {
        backgroundColor: '#ced4e5'
      } : {
        backgroundColor: '#e8ebf5'
      }
    }, e.map((f, i) => {
      return /*#__PURE__*/_react.default.createElement(TdRow, {
        key: i,
        "data-testid": 't-' + indx + '-' + i,
        props: headers.length
      }, typeof f === 'boolean' ? f.toString() : f);
    }));
  }))) : /*#__PURE__*/_react.default.createElement(ErrorSpan, {
    "data-testid": "spanError"
  }, "Il n'y a pas de donn\xE9es, ou le nombre de donn\xE9es ne correspond pas aux en-t\xEAtes"), /*#__PURE__*/_react.default.createElement(BottomDiv, null, (data === null || data === void 0 ? void 0 : data.length) > recordsPerPage ? /*#__PURE__*/_react.default.createElement(Pagination, {
    numberPerPage: recordsPerPage,
    dataCount: data.length,
    onPaginationEvent: e => setCurrentPage(e),
    debug: payload.debug
  }) : '', (data === null || data === void 0 ? void 0 : data.length) > 0 ? /*#__PURE__*/_react.default.createElement(Select, {
    defaultValue: 10,
    name: "stackSize",
    "data-testid": "selectSize",
    onChange: e => setRecordsPerPage(e.target.value)
  }, /*#__PURE__*/_react.default.createElement("option", {
    value: "5"
  }, "5"), /*#__PURE__*/_react.default.createElement("option", {
    value: "10"
  }, "10"), /*#__PURE__*/_react.default.createElement("option", {
    value: "20"
  }, "20"), /*#__PURE__*/_react.default.createElement("option", {
    value: "50"
  }, "50")) : ''));
}
const ContainerInput = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n  position: relative;\n  margin-bottom: 10px;\n"])));
const InputSearch = _styledComponents.default.input(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral([""])));
const ErrorSpan = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  font-size: 25px;\n  color: red;\n"])));
const UL = _styledComponents.default.ul(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: row;\n"])));
const DIV = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  margin: auto;\n"])));
const LI = _styledComponents.default.li(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n  cursor: pointer;\n  list-style: none;\n  text-decoration: none;\n  margin: 0 10px;\n  padding: 5px;\n  border-bottom: 2px solid #5996e3;\n"])));
const LICurrent = _styledComponents.default.li(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n  cursor: pointer;\n  list-style: none;\n  text-decoration: none;\n  margin: 0 10px;\n  padding: 5px;\n  border-bottom: 4px solid #63d0fd;\n"])));
function Pagination(_ref) {
  let {
    numberPerPage,
    dataCount,
    onPaginationEvent,
    debug,
    currentPage
  } = _ref;
  if (debug) console.log(dataCount, numberPerPage);
  const [currP, setCurrP] = (0, _react.useState)(0);
  let nbrePage = Math.ceil(dataCount / numberPerPage);
  let pages = [];
  for (let i = 0; i < nbrePage; i++) {
    pages.push(i);
  }
  if (currentPage == 0) {
    setCurrP(1);
  }
  if (debug) console.log(pages);
  return /*#__PURE__*/_react.default.createElement(DIV, null, /*#__PURE__*/_react.default.createElement(UL, null, pages.map((p, i) => {
    if (p === currP) {
      return /*#__PURE__*/_react.default.createElement(LICurrent, {
        key: i,
        "data-testid": "li".concat(i),
        onClick: () => {
          onPaginationEvent(Math.max(currP - 2, 0));
          setCurrP(p);
        }
      }, p + 1);
    } else {
      if (p === 0 || p === pages.length - 1) {
        return /*#__PURE__*/_react.default.createElement(LI, {
          "data-testid": "li".concat(i),
          key: i,
          onClick: () => {
            onPaginationEvent(p);
            setCurrP(p);
          }
        }, i + 1);
      }
      if (p > currP + 4) {
        return;
      }
      if (p === currP + 4) {
        return /*#__PURE__*/_react.default.createElement(LI, {
          key: i,
          onClick: () => {
            onPaginationEvent(Math.min(currP + 2, pages.length - 1));
            setCurrP(p);
          }
        }, "...");
      }
      if (p === currP - 4) {
        return /*#__PURE__*/_react.default.createElement(LI, {
          key: i,
          onClick: () => {
            onPaginationEvent(Math.max(currP - 2, 0));
            setCurrP(p);
          }
        }, "...");
      }
      if (p < currP - 4) {
        return;
      }
      return /*#__PURE__*/_react.default.createElement(LI, {
        key: i,
        onClick: () => {
          onPaginationEvent(i);
          setCurrP(p);
        }
      }, p + 1);
    }
  })));
}
const LiRow = _styledComponents.default.li(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral([""])));
exports.LiRow = LiRow;
const UlRow = _styledComponents.default.ul(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral([""])));
exports.UlRow = UlRow;
const ThRow = _styledComponents.default.th(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n  border: 'none';\n  padding: 15px;\n  color: #d9d9d9;\n"])));
exports.ThRow = ThRow;
const Select = _styledComponents.default.select(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n  border-radius: 3px;\n"])));
exports.Select = Select;
const Button = _styledComponents.default.button(_templateObject12 || (_templateObject12 = _taggedTemplateLiteral(["\n  border: none;\n  background-color: transparent;\n  cursor: pointer;\n"])));
exports.Button = Button;
const THead = _styledComponents.default.thead(_templateObject13 || (_templateObject13 = _taggedTemplateLiteral(["\n  border: 'none';\n\n  padding: 5px;\n  margin: 5px;\n"])));
exports.THead = THead;
const Table = _styledComponents.default.table(_templateObject14 || (_templateObject14 = _taggedTemplateLiteral(["\n  border: 'none';\n"])));
exports.Table = Table;
const Main = _styledComponents.default.div(_templateObject15 || (_templateObject15 = _taggedTemplateLiteral(["\n  width: 60%;\n  display: flex;\n  flex-direction: column;\n  margin: auto;\n"])));
exports.Main = Main;
const TdRow = _styledComponents.default.td(_templateObject16 || (_templateObject16 = _taggedTemplateLiteral(["\n  border: 'none';\n  padding: 15px;\n  width: ", ";\n"])), props => props.props ? 'calc(' + 100 / props.props + '%)' : 'auto');
exports.TdRow = TdRow;
const BottomDiv = _styledComponents.default.div(_templateObject17 || (_templateObject17 = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  margin-top: 10px;\n"])));
exports.BottomDiv = BottomDiv;