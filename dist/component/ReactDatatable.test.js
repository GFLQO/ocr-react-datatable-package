"use strict";

require("core-js/modules/es.weak-map.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.promise.js");
var _react = _interopRequireDefault(require("react"));
var _react2 = require("@testing-library/react");
var _ReactDataTable = _interopRequireWildcard(require("./ReactDataTable"));
require("@testing-library/jest-dom/extend-expect");
var _userEvent = _interopRequireDefault(require("@testing-library/user-event"));
var _reactTestRenderer = require("react-test-renderer");
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
describe('ReactDataTable', () => {
  it('ReactDataTable displays data correctly', () => {
    const default_sens = 1;
    const default_Active = false;
    const headers = [{
        name: 'S1',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'S2',
        sens: default_sens,
        type: 'number',
        isActive: default_Active
      }, {
        name: 'S3',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'name',
        sens: default_sens,
        type: 'boolean',
        isActive: default_Active
      }],
      data = [['B1DATA', 83, 'S3DATA', true], ['A1DfreATA', 42, 'K3DATferA', false], ['gzzzre', 95, 'ff', true], ['gr-ye', 4, 'ff', false]];
    const payload = {
      headers,
      data
    };
    const {
      getByTestId
    } = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_ReactDataTable.default, {
      headers: payload.headers,
      data: payload.data
    }));
    expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('B1DATA');
    expect(_react2.screen.getByTestId('t-0-1').textContent).toBe('83');
  }), it('should sort the data by the column that is clicked on', async () => {
    const default_sens = 1;
    const default_Active = false;
    const headers = [{
        name: 'S1',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'S2',
        sens: default_sens,
        type: 'number',
        isActive: default_Active
      }, {
        name: 'S3',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'name',
        sens: default_sens,
        type: 'boolean',
        isActive: default_Active
      }],
      data = [['B1DATA', 83, 'S3DATA', true], ['A1DfreATA', 42, 'K3DATferA', false], ['gzzzre', 95, 'ff', true], ['gr-ye', 4, 'ff', false]];
    const payload = {
      headers,
      data
    };
    const {
      getByText
    } = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_ReactDataTable.default, {
      headers: payload.headers,
      data: payload.data
    }));
    (0, _reactTestRenderer.act)(() => {
      _react2.fireEvent.click(getByText('S1'));
    });
    await (0, _react2.waitFor)(() => {
      // click on the 'Name' header button
      const columnheader = _react2.screen.getByRole('columnheader', {
        name: /s1/i
      });
      expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('B1DATA');

      // simulate click on column header
      _react2.fireEvent.click(_react2.screen.getByTestId('but0'));
      _react2.fireEvent.click(_react2.screen.getByTestId('but0'));
      // check if the data is sorted by the 'Name' column
      expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('A1DfreATA');
      // simulate click on column header
      _react2.fireEvent.click(_react2.screen.getByTestId('but0'));
      // check if the data is sorted by the 'Name' column
      expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('gzzzre');
    });
  });
});
describe('ReactDataTable Pagination', () => {
  it('Pagination should get the data and build pages as much as needed ', () => {
    const default_sens = 1;
    const default_Active = false;
    const headers = [{
        name: 'S1',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'S2',
        sens: default_sens,
        type: 'number',
        isActive: default_Active
      }, {
        name: 'S3',
        sens: default_sens,
        type: 'string',
        isActive: default_Active
      }, {
        name: 'name',
        sens: default_sens,
        type: 'boolean',
        isActive: default_Active
      }],
      data = [['B1DATA', 83, 'S3DATA', true], ['A1DfreATA', 42, 'K3DATferA', false], ['gzzzre', 95, 'ff', true], ['gr-ye', 4, 'ff', false], ['B1DATA', 83, 'S3DATA', true], ['A1DfreATA', 42, 'K3DATferA', false], ['gzzzre', 95, 'ff', true], ['gr-ye', 4, 'ff', false], ['B1DATA', 83, 'S3DATA', true], ['A1DfreATA', 42, 'K3DATferA', false], ['gzzzre', 95, 'ff', true], ['gr-ye', 4, 'ff', false]];
    const payload = {
      headers,
      data
    };
    const {
      getByTestId
    } = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_ReactDataTable.default, {
      headers: payload.headers,
      data: payload.data
    }));
    expect(_react2.screen.getByTestId('li0')).toBeDefined();
    expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('B1DATA');
    expect(_react2.screen.getByTestId('t-0-1').textContent).toBe('83');
    (0, _reactTestRenderer.act)(() => {
      //Click on second page
      _react2.fireEvent.click(_react2.screen.getByTestId('li1'));
    });
    console.log(_react2.screen.logTestingPlaygroundURL());

    //Expect second page is loaded
    expect(_react2.screen.getByTestId('t-0-0').textContent).toBe('gzzzre');
    let select = _react2.screen.getByTestId('selectSize');
    _userEvent.default.selectOptions(select, '50');
    expect(select.value).toBe('50');
    expect(_react2.screen.queryByTestId('li0')).toBe(null);
  });
});
describe('No entries reaction', () => {
  it('should display error message if no data', () => {
    const headers = [],
      data = [];
    const payload = {
      headers,
      data
    };
    const {
      getByTestId
    } = (0, _react2.render)( /*#__PURE__*/_react.default.createElement(_ReactDataTable.default, {
      headers: payload.headers,
      data: payload.data
    }));
    expect(_react2.screen.getByTestId('spanError').textContent).toBe("Il n'y a pas de données, ou le nombre de données ne correspond pas aux en-têtes");
  });
});