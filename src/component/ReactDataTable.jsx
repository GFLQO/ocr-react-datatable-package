/**
 * It's a React component that takes a payload as a parameter and returns a table
 */
import React, { useState } from 'react';
import styled from 'styled-components';

/* A function that is exported. */
export default function ReactDataTable(payload) {
  const [currentPage, setCurrentPage] = useState(0);
  const [recordsPerPage, setRecordsPerPage] = useState(10);
  const [active, setActive] = useState(0);
  const [data, setData] = useState(payload.data);
  const [headers, setHeaders] = useState(payload.headers);
  let isError = false;
  if (payload.hasOwnProperty('headers') && payload.hasOwnProperty('data')) {
    if (
      payload.data.length > 0 &&
      payload.headers.length !== payload.data[0].length
    ) {
      isError = true;
      console.log('Données non conformes');
    }
  }
  if (payload.debug) console.log('headers :', headers, 'data : ', data);

  function searchBy(searchTerm) {
    if (searchTerm !== '') {
      const filteredData = data.filter((record) =>
        record.some((value) => value.toString().includes(searchTerm)),
      );
      setData(filteredData);
    } else {
      setData(payload.data);
    }
  }

  function sortBy(element) {
    if (payload.debug) console.log(element, headers);
    headers.forEach((h) => {
      if (h.name === element.name) {
        h.isActive = true;
      } else {
        h.isActive = false;
      }
    });
    const indexToSort = headers.indexOf(element);
    element.sens *= -1;
    const sens = element.sens;
    const type = element.type;
    if (type === 'string') {
      data.sort((a, b) => sens * a[indexToSort].localeCompare(b[indexToSort]));
    } else if (type === 'number') {
      data.sort((a, b) => sens * (a[indexToSort] - b[indexToSort]));
    } else if (type === 'boolean') {
      data.sort((a, b) => sens * a[indexToSort] - b[indexToSort]);
      if (payload.debug) console.log('triage');
    }
    setData(data);
    setHeaders(headers);
    setActive(active + 1);
  }
  return (
    <Main>
      <ContainerInput>
        <InputSearch onChange={(e) => searchBy(e.target.value)} />
      </ContainerInput>
      {data && data.length > 0 && !isError ? (
        <Table>
          <THead>
            <tr>
              {headers?.map((element, index) => {
                return (
                  <ThRow
                    key={index}
                    data-testid={`h${index}`}
                    style={
                      element.isActive
                        ? {
                            backgroundColor: '#1F3140',
                            color: 'white',
                            borderBottom: '5px solid #09A',
                            textUnderlineOffset: '8px',
                            textDecorationThickness: '3px',
                            boxSizing: 'border-box',
                          }
                        : {
                            backgroundColor: '#1F3140',
                            borderBottom: '5px solid transparent',
                          }
                    }
                  >
                    {element.name}{' '}
                    <Button
                      data-testid={`but${index}`}
                      onClick={() => sortBy(element)}
                    >
                      {element.sens === 1 ? (
                        <span style={{ color: 'white' }}>▲</span>
                      ) : (
                        <span style={{ color: 'white' }}>▼</span>
                      )}
                    </Button>{' '}
                  </ThRow>
                );
              })}
            </tr>
          </THead>
          <tbody>
            {data
              .slice(
                currentPage * recordsPerPage,
                Math.min(data.length, (currentPage + 1) * recordsPerPage),
              )
              .map((e, indx) => {
                return (
                  <tr
                    key={indx}
                    style={
                      indx % 2 === 0
                        ? { backgroundColor: '#ced4e5' }
                        : { backgroundColor: '#e8ebf5' }
                    }
                  >
                    {e.map((f, i) => {
                      return (
                        <TdRow
                          key={i}
                          data-testid={'t-' + indx + '-' + i}
                          props={headers.length}
                        >
                          {typeof f === 'boolean' ? f.toString() : f}
                        </TdRow>
                      );
                    })}
                  </tr>
                );
              })}
          </tbody>
        </Table>
      ) : (
        <ErrorSpan data-testid="spanError">
          Il n'y a pas de données, ou le nombre de données ne correspond pas aux
          en-têtes
        </ErrorSpan>
      )}
      <BottomDiv>
        {data?.length > recordsPerPage ? (
          <Pagination
            numberPerPage={recordsPerPage}
            dataCount={data.length}
            onPaginationEvent={(e) => setCurrentPage(e)}
            debug={payload.debug}
          />
        ) : (
          ''
        )}
        {data?.length > 0 ? (
          <Select
            defaultValue={10}
            name="stackSize"
            data-testid="selectSize"
            onChange={(e) => setRecordsPerPage(e.target.value)}
          >
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </Select>
        ) : (
          ''
        )}
      </BottomDiv>
    </Main>
  );
}

const ContainerInput = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  position: relative;
  margin-bottom: 10px;
`;
const InputSearch = styled.input``;
const ErrorSpan = styled.span`
  font-size: 25px;
  color: red;
`;

const UL = styled.ul`
  display: flex;
  flex-direction: row;
`;

const DIV = styled.div`
  margin: auto;
`;
const LI = styled.li`
  cursor: pointer;
  list-style: none;
  text-decoration: none;
  margin: 0 10px;
  padding: 5px;
  border-bottom: 2px solid #5996e3;
`;

const LICurrent = styled.li`
  cursor: pointer;
  list-style: none;
  text-decoration: none;
  margin: 0 10px;
  padding: 5px;
  border-bottom: 4px solid #63d0fd;
`;

export function Pagination({
  numberPerPage,
  dataCount,
  onPaginationEvent,
  debug,
  currentPage,
}) {
  if (debug) console.log(dataCount, numberPerPage);
  const [currP, setCurrP] = useState(0);
  let nbrePage = Math.ceil(dataCount / numberPerPage);
  let pages = [];
  for (let i = 0; i < nbrePage; i++) {
    pages.push(i);
  }
  if (currentPage == 0) {
    setCurrP(1);
  }

  if (debug) console.log(pages);
  return (
    <DIV>
      <UL>
        {pages.map((p, i) => {
          if (p === currP) {
            return (
              <LICurrent
                key={i}
                data-testid={`li${i}`}
                onClick={() => {
                  onPaginationEvent(Math.max(currP - 2, 0));
                  setCurrP(p);
                }}
              >
                {p + 1}
              </LICurrent>
            );
          } else {
            if (p === 0 || p === pages.length - 1) {
              return (
                <LI
                  data-testid={`li${i}`}
                  key={i}
                  onClick={() => {
                    onPaginationEvent(p);
                    setCurrP(p);
                  }}
                >
                  {i + 1}
                </LI>
              );
            }
            if (p > currP + 4) {
              return;
            }
            if (p === currP + 4) {
              return (
                <LI
                  key={i}
                  onClick={() => {
                    onPaginationEvent(Math.min(currP + 2, pages.length - 1));
                    setCurrP(p);
                  }}
                >
                  ...
                </LI>
              );
            }
            if (p === currP - 4) {
              return (
                <LI
                  key={i}
                  onClick={() => {
                    onPaginationEvent(Math.max(currP - 2, 0));
                    setCurrP(p);
                  }}
                >
                  ...
                </LI>
              );
            }
            if (p < currP - 4) {
              return;
            }

            return (
              <LI
                key={i}
                onClick={() => {
                  onPaginationEvent(i);
                  setCurrP(p);
                }}
              >
                {p + 1}
              </LI>
            );
          }
        })}
      </UL>
    </DIV>
  );
}

export const LiRow = styled.li``;

export const UlRow = styled.ul``;

export const ThRow = styled.th`
  border: 'none';
  padding: 15px;
  color: #d9d9d9;
`;

export const Select = styled.select`
  border-radius: 3px;
`;

export const Button = styled.button`
  border: none;
  background-color: transparent;
  cursor: pointer;
`;

export const THead = styled.thead`
  border: 'none';

  padding: 5px;
  margin: 5px;
`;

export const Table = styled.table`
  border: 'none';
`;

export const Main = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  margin: auto;
`;

export const TdRow = styled.td`
  border: 'none';
  padding: 15px;
  width: ${(props) =>
    props.props ? 'calc(' + 100 / props.props + '%)' : 'auto'};
`;

export const BottomDiv = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin-top: 10px;
`;
