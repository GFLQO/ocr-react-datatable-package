import React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import ReactDataTable, { Pagination } from './ReactDataTable';
import '@testing-library/jest-dom/extend-expect';
import userEvent from '@testing-library/user-event';
import { act } from 'react-test-renderer';

describe('ReactDataTable', () => {
  it('ReactDataTable displays data correctly', () => {
    const default_sens = 1;
    const default_Active = false;
    const headers = [
        {
          name: 'S1',
          sens: default_sens,
          type: 'string',
          isActive: default_Active,
        },
        {
          name: 'S2',
          sens: default_sens,
          type: 'number',
          isActive: default_Active,
        },
        {
          name: 'S3',
          sens: default_sens,
          type: 'string',
          isActive: default_Active,
        },
        {
          name: 'name',
          sens: default_sens,
          type: 'boolean',
          isActive: default_Active,
        },
      ],
      data = [
        ['B1DATA', 83, 'S3DATA', true],
        ['A1DfreATA', 42, 'K3DATferA', false],
        ['gzzzre', 95, 'ff', true],
        ['gr-ye', 4, 'ff', false],
      ];

    const payload = { headers, data };

    const { getByTestId } = render(
      <ReactDataTable headers={payload.headers} data={payload.data} />,
    );
    expect(screen.getByTestId('t-0-0').textContent).toBe('B1DATA');
    expect(screen.getByTestId('t-0-1').textContent).toBe('83');
  }),
    it('should sort the data by the column that is clicked on', async () => {
      const default_sens = 1;
      const default_Active = false;
      const headers = [
          {
            name: 'S1',
            sens: default_sens,
            type: 'string',
            isActive: default_Active,
          },
          {
            name: 'S2',
            sens: default_sens,
            type: 'number',
            isActive: default_Active,
          },
          {
            name: 'S3',
            sens: default_sens,
            type: 'string',
            isActive: default_Active,
          },
          {
            name: 'name',
            sens: default_sens,
            type: 'boolean',
            isActive: default_Active,
          },
        ],
        data = [
          ['B1DATA', 83, 'S3DATA', true],
          ['A1DfreATA', 42, 'K3DATferA', false],
          ['gzzzre', 95, 'ff', true],
          ['gr-ye', 4, 'ff', false],
        ];

      const payload = { headers, data };

      const { getByText } = render(
        <ReactDataTable headers={payload.headers} data={payload.data} />,
      );
      act(() => {
        fireEvent.click(getByText('S1'));
      });

      await waitFor(() => {
        // click on the 'Name' header button
        const columnheader = screen.getByRole('columnheader', {
          name: /s1/i,
        });

        expect(screen.getByTestId('t-0-0').textContent).toBe('B1DATA');

        // simulate click on column header
        fireEvent.click(screen.getByTestId('but0'));
        fireEvent.click(screen.getByTestId('but0'));
        // check if the data is sorted by the 'Name' column
        expect(screen.getByTestId('t-0-0').textContent).toBe('A1DfreATA');
        // simulate click on column header
        fireEvent.click(screen.getByTestId('but0'));
        // check if the data is sorted by the 'Name' column
        expect(screen.getByTestId('t-0-0').textContent).toBe('gzzzre');
      });
    });
});

describe('ReactDataTable Pagination', () => {
  it('Pagination should get the data and build pages as much as needed ', () => {
    const default_sens = 1;
    const default_Active = false;
    const headers = [
        {
          name: 'S1',
          sens: default_sens,
          type: 'string',
          isActive: default_Active,
        },
        {
          name: 'S2',
          sens: default_sens,
          type: 'number',
          isActive: default_Active,
        },
        {
          name: 'S3',
          sens: default_sens,
          type: 'string',
          isActive: default_Active,
        },
        {
          name: 'name',
          sens: default_sens,
          type: 'boolean',
          isActive: default_Active,
        },
      ],
      data = [
        ['B1DATA', 83, 'S3DATA', true],
        ['A1DfreATA', 42, 'K3DATferA', false],
        ['gzzzre', 95, 'ff', true],
        ['gr-ye', 4, 'ff', false],
        ['B1DATA', 83, 'S3DATA', true],
        ['A1DfreATA', 42, 'K3DATferA', false],
        ['gzzzre', 95, 'ff', true],
        ['gr-ye', 4, 'ff', false],
        ['B1DATA', 83, 'S3DATA', true],
        ['A1DfreATA', 42, 'K3DATferA', false],
        ['gzzzre', 95, 'ff', true],
        ['gr-ye', 4, 'ff', false],
      ];

    const payload = { headers, data };

    const { getByTestId } = render(
      <ReactDataTable headers={payload.headers} data={payload.data} />,
    );
    expect(screen.getByTestId('li0')).toBeDefined();
    expect(screen.getByTestId('t-0-0').textContent).toBe('B1DATA');
    expect(screen.getByTestId('t-0-1').textContent).toBe('83');

    act(() => {
      //Click on second page
      fireEvent.click(screen.getByTestId('li1'));
    });
    console.log(screen.logTestingPlaygroundURL());

    //Expect second page is loaded
    expect(screen.getByTestId('t-0-0').textContent).toBe('gzzzre');
    let select = screen.getByTestId('selectSize');
    userEvent.selectOptions(select, '50');
    expect(select.value).toBe('50');

    expect(screen.queryByTestId('li0')).toBe(null);
  });
});

describe('No entries reaction', () => {
  it('should display error message if no data', () => {
    const headers = [],
      data = [];
    const payload = { headers, data };
    const { getByTestId } = render(
      <ReactDataTable headers={payload.headers} data={payload.data} />,
    );

    expect(screen.getByTestId('spanError').textContent).toBe(
      "Il n'y a pas de données, ou le nombre de données ne correspond pas aux en-têtes",
    );
  });
});
