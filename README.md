# OCR-React-Datatable

OCR-React-Datatable is a React component that renders a sortable and paginated table. It can be customized with the headers and data that you provide.

## Installation

You can install the package using npm:

```
npm install ocr-react-datatable
```

## Usage

```js
import ReactDataTable from 'ocr-react-datatable';

const data = [
  [1, 'John', 'Doe'],
  [2, 'Jane', 'Doe'],
  [3, 'Bob', 'Smith'],
  [4, 'Alice', 'Johnson'],
];

const headers = [
  { name: 'ID', type: 'number', isActive: false, sens: 1 },
  { name: 'First Name', type: 'string', isActive: false, sens: 1 },
  { name: 'Last Name', type: 'string', isActive: false, sens: 1 },
];

function App() {
  return (
    <div>
      <h1>OCR React Datatable</h1>
      <ReactDataTable data={data} headers={headers} />
    </div>
  );
}

export default App;
```

## API

### ReactDataTable

The ReactDataTable component takes in the following props:

#### data

Type: Array

An array of arrays containing the data to be displayed in the table.

#### headers

Type: Array

An array of objects representing the headers of the table. Each object should have a name, type, isActive, and sens property.

name: The name of the header
type: The type of data in the column. Currently supported types are number, string, and boolean.
isActive: Whether the column is currently being sorted by.
sens: The direction to sort the column by. 1 for ascending and -1 for descending.
debug
Type: Boolean

A flag indicating whether to output debug information to the console.

## Contributing

We welcome contributions to OCR React Datatable! To get started, clone the repository and install the dependencies:

```
git clone https://github.com/your-username/ocr-react-datatable.git
cd ocr-react-datatable
npm install
```

Then, you can run the tests with:

```
npm test
```

## License

OCR React Datatable is licensed under the MIT License.
